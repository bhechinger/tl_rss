import psycopg2, psycopg2.extensions, psycopg2.extras
import logging, configparser
from datetime import datetime
from datetime import timedelta


class tldb:
    def __init__(self, config_file, debug=False):
        self.logger = logging.getLogger(__name__)
        self.logger.info('creating an instance of tldb')
        self.debug = debug
        self.max_episode = 99  # This just needs to be silly high really

        config = configparser.ConfigParser()
        config.read(config_file)

        dsn = dict()
        dsn['host'] = 'localhost'
        dsn['dbname'] = None
        dsn['user'] = None
        dsn['password'] = None
        dsn['port'] = 5432

        for option in dsn:
            try:
                dsn[option] = config.get('database', option)
            except configparser.NoOptionError:
                # Anything with defaults set isn't required
                if dsn[option] is None:
                    self.logger.error("option {o} is required!".format(o=option))
                    return

        DSN = ''
        for option in dsn:
            DSN += "{k}={v} ".format(k=option, v=dsn[option])

        self.conn = psycopg2.connect(DSN)
        self.conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

    def get_cursor(self):
        return self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    def set_run(self):
        cur = self.get_cursor()
        now = datetime.now()
        cur.execute("UPDATE options set value = %s where name = 'rss_last_checked'", (now,))
        if cur.rowcount == 0:
            cur.execute("INSERT into options (name, value) VALUES ('rss_last_checked', %s)", (now,))
        cur.close()
        return now

    def can_run(self, run_interval):
        self.logger.debug("Entering can_run()")
        cur = self.get_cursor()
        cur.execute("SELECT value FROM options WHERE name = 'rss_last_checked'")
        if cur.rowcount == 0:
            last_checked = self.set_run()
        else:
            rss_last_checked = cur.fetchone()[0]
            self.logger.debug("rss_last_checked date: {d}".format(d=rss_last_checked))
            last_checked = datetime.strptime(rss_last_checked, '%Y-%m-%d %H:%M:%S.%f')

        now_minus_interval = datetime.now() - timedelta(seconds=run_interval * 60)
        self.logger.info("last checked: {l}".format(l=last_checked))
        self.logger.debug("now minus interval: {n}".format(n=now_minus_interval))

        cur.close()
        if now_minus_interval >= last_checked:
            return True

        return False

    def remove_show(self, name):
        exists, db_name = self.get_show(name)
        if not exists:
            return 0

        cur = self.get_cursor()
        cur.execute("""DELETE FROM episodes WHERE show = (SELECT id FROM shows WHERE name = %s)""", (db_name,))
        rowcount = cur.rowcount
        cur.execute("""DELETE FROM shows WHERE name = %s""", (db_name,))
        cur.close()
        self.logger.info("Deleted show {n} which had {r} rows".format(n=name, r=rowcount))
        return rowcount

    def add_show(self, name, season, episode, force=False):
        exists, db_name = self.get_show(name)
        cur = self.get_cursor()
        cur.execute("SELECT * FROM shows where name = %s", (db_name,))
        if cur.rowcount != 0 and not force:
            self.logger.warning("Show exists")
            cur.close()
            return 0

        if cur.rowcount != 0 and force:
            # It's easiest to just kill all this and start over
            self.logger.debug("Removing {n} from DB before adding new".format(n=name))
            self.remove_show(name)

        try:
            cur.execute("""INSERT INTO shows (name) VALUES (%s) RETURNING id""", (name,))
            show_id = cur.fetchone()[0]
        except psycopg2.IntegrityError:
            self.logger.warning("Show exists")
            cur.close()
            return 0

        self.logger.debug("Adding seasons: 1 - {0}".format(season))

        params = list()
        for s in range(1, season + 1):
            self.logger.debug("Adding season: {0}".format(s))

            if s != season:
                for e in range(1, self.max_episode + 1):
                    params.append((show_id, s, e))
            else:
                for e in range(1, episode + 1):
                    params.append((show_id, s, e))

        try:
            cur.executemany("INSERT INTO episodes (show, season, episode) VALUES (%s, %s, %s)", params)
            rowcount = cur.rowcount
        except psycopg2.IntegrityError:
            self.logger.warning("Show exists")
            rowcount = 0

        cur.close()
        return rowcount

    def add_episode(self, name, season, episode):
        cur = self.get_cursor()

        try:
            cur.execute("""INSERT INTO episodes (show, season, episode) VALUES
                        ((SELECT id FROM shows WHERE name = %s), %s, %s)""", (name, season, episode))
            rowcount = cur.rowcount
        except psycopg2.IntegrityError:
            self.logger.warning("Episode exists")
            rowcount = 0

        cur.close()
        return rowcount

    def get_show(self, name):
        cur = self.get_cursor()
        cur.execute("SELECT * FROM shows")
        for show in cur.fetchall():
            if show[1].lower() in name.lower():
                cur.close()
                return True, show[1]

        cur.close()
        return False, None

    def have_episode(self, name, season, episode):
        exists, db_name = self.get_show(name)
        if not exists:
            # Show doesn't even exist, so the episide can't!
            return False

        cur = self.get_cursor()
        cur.execute("""SELECT * FROM episodes LEFT JOIN shows ON episodes.show=shows.id WHERE shows.name = %s
                    AND season = %s AND episode = %s""", (db_name, season, episode))
        if cur.rowcount == 0:
            cur.close()
            return False

        cur.close()
        return True

    def list_shows(self):
        cur = self.get_cursor()
        cur.execute("""SELECT shows.name, MAX(episodes.season)
                       FROM episodes LEFT JOIN shows ON episodes.show=shows.id
                       WHERE shows.active = 't' GROUP BY shows.name""")

        for show in cur.fetchall():
            cur.execute("""SELECT MAX(episodes.episode)
                           FROM episodes LEFT JOIN shows ON episodes.show=shows.id WHERE shows.name = %s
                           AND season = %s""", (show[0], show[1]))
            print("{n} S{s:02d}E{e:02d}".format(n=show[0], s=show[1], e=cur.fetchone()[0]))

        cur.close()
