CREATE TABLE shows (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255),
  active BOOLEAN DEFAULT 'true',
	UNIQUE (name)
);

CREATE TABLE episodes (
  id SERIAL PRIMARY KEY,
  show INTEGER REFERENCES shows(id),
	season INTEGER,
	episode INTEGER,
  UNIQUE (show, season, episode)
);

CREATE TABLE options (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255),
	value VARCHAR(255),
  UNIQUE (name)
);
