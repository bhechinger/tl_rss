import requests, re, transmissionrpc, logging, os
from tldb import tldb
from lxml import etree
from time import sleep

try:
    import ConfigParser as configparser
except ImportError:
    import configparser


class RSS_Feeder:
    def __init__(self, args):
        # Let's store this for access from elsewhere
        self.args = args

        if args.config:
            config_file = args.config
        else:
            config_file = 'tl_rss.conf'

        config = configparser.ConfigParser()
        config.read(config_file)

        try:
            self.sleep_interval = float(config.get('global', 'sleep_interval'))
        except configparser.NoOptionError:
            self.sleep_interval = 60

        try:
            self.pid_file = config.get('global', 'pid_file')
        except configparser.NoOptionError:
            self.pid_file = "/tmp/tl_rss.pid"

        try:
            self.log_file = config.get('logging', 'log_file')
        except configparser.NoOptionError:
            self.log_file = "/var/log/tl_rss.log"

        try:
            self.log_level = getattr(logging, config.get('logging', 'log_level').upper())
        except configparser.NoOptionError:
            self.log_level = logging.WARNING

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        self.formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')

        self.log_fh = logging.FileHandler(self.log_file)
        self.log_fh.setLevel(self.log_level)
        self.log_fh.setFormatter(self.formatter)
        self.logger.addHandler(self.log_fh)

        if self.args.debug:
            ch = logging.StreamHandler()
            ch.setLevel(logging.DEBUG)
            ch.setFormatter(self.formatter)
            self.logger.addHandler(ch)

        try:
            self.tc = transmissionrpc.Client(
                config.get('transmission', 'hostname'),
                port=config.get('transmission', 'port'),
                user=config.get('transmission', 'username'),
                password=config.get('transmission', 'password')
            )
        except Exception as e:
            self.logger.error("Transmission Error: {0}".format(e))
            return

        self.logger.debug("Setting up tldb")
        self.db = tldb(config_file, debug=args.debug)
        self.logger.debug("Done setting up tldb")
        self.run_interval = int(config.get('global', 'run_interval'))
        self.rss_url = 'http://rss.torrentleech.org/{0}'.format(config.get('global', 'api_key'))

    def run_once(self):
        if self.args.once:
            self.logger.debug("Running once...")
        else:
            self.logger.debug("run_once() called from loop")

        if self.db.can_run(self.run_interval):
            self.db.set_run()
        else:
            self.logger.debug("It's been less than {0} minutes since last run, aborting!".format(self.run_interval))
            return

        self.logger.info("Checking for new shows")

        try:
            rss_feed = requests.get(self.rss_url)
        except Exception as e:
            self.logger.error("HTTP Error: {0}".format(e))
            return

        tree = etree.fromstring(rss_feed.content)

        for item in tree.xpath('//item'):
            link = item.xpath('link')[0].text
            category = item.xpath('category')[0].text
            #description = item.xpath('description')[0].text
            title = item.xpath('title')[0].text
            # self.logger.debug("Checking: {title} :: {category}".format(title=title, category=category))

            if category == 'Episodes HD':
                self.logger.debug("We have a category we want, checking: {title}".format(title=title))
                # I still don't actually use this, but I figured out how to parse this so I refuse to delete it
                # desc = description.split(' - ')
                # seeders = int(desc[1].split(':')[-1].strip())
                # leechers = int(desc[2].split(':')[-1].strip())

                m = re.search('(^.+ )([Ss]\d+)([Ee]\d+)', title)
                if m:
                    show_name = m.group(1).strip()
                    self.logger.debug("It's a show: {title}".format(title=title))
                    do_get_show, db_show_name = self.db.get_show(show_name)
                    if do_get_show:
                        season = int(m.group(2)[1:])
                        episode = int(m.group(3)[1:])
                        self.logger.debug(
                            "It's a show we watch: \"{name}\" {s} {e}".format(name=show_name, s=season, e=episode))
                        if not self.db.have_episode(show_name, season, episode):
                            self.logger.info("Fetching: {0} S{1:02d}E{2:02d}".format(show_name, season, episode))
                            try:
                                if self.args.debug:
                                    self.logger.debug(
                                        "Adding to transmission server: {l}".format(l=link))
                                torrent_file = requests.get(link)
                                torrent_file_name = '/tmp/{show} {season} {episode}.torrent'.format(show=show_name, season=season, episode=episode)
                                with open(torrent_file_name, 'wb') as fh:
                                    fh.write(torrent_file.content)
                                self.tc.add_torrent('file://{name}'.format(name=torrent_file_name))
                                os.remove(torrent_file_name)
                                self.db.add_episode(db_show_name, season, episode)
                            except transmissionrpc.error.TransmissionError as e:
                                self.logger.warning('Transmission Error: {0}'.format(e))
                        else:
                            self.logger.info("Already have: {0} S{1:02d}E{2:02d}".format(show_name, season, episode))

    def run_loop(self):
        while True:
            self.run_once()
            try:
                sleep(self.sleep_interval)
            except KeyboardInterrupt:
                self.logger.warning("Caught keyboard interrupt, exiting")
                return


if __name__ == "__main__":
    raise SystemExit("No, just no.")
