#!/usr/bin/env python3
import re, os, rarfile, glob, shutil
from sparkpost import SparkPost

rarfile.NEED_COMMENTS = 0
rarfile.UNICODE_COMMENTS = 1

ext_list = ['avi', 'AVI', 'divx', 'flv',
            'iso', 'm4v', 'mkv', 'mov',
            'mp3', 'mp4', 'mpeg', 'mpg',
            'MPG', 'srt', 'wmv']

def debug_msg(message):
    with open('/tmp/TR.out', 'a+') as f:
        f.write('{m}\n'.format(m=message))

debug_msg("***** Starting! *****")

TR = dict()
TR['app_version'] = os.getenv('TR_APP_VERSION')
TR['time'] = os.getenv('TR_TIME_LOCALTIME')
TR['torrent_dir'] = os.getenv('TR_TORRENT_DIR')
TR['torrent_hash'] = os.getenv('TR_TORRENT_HASH')
TR['torrent_id'] = os.getenv('TR_TORRENT_ID')
TR['torrent_name'] = os.getenv('TR_TORRENT_NAME')
torrent_path = '{tdir}/{tname}'.format(tdir=TR['torrent_dir'], tname=TR['torrent_name'])

debug_msg('app_version: {foo}'.format(foo = TR['app_version']))
debug_msg('time: {foo}'.format(foo = TR['time']))
debug_msg('torrent_dir: {foo}'.format(foo = TR['torrent_dir']))
debug_msg('torrent_hash: {foo}'.format(foo = TR['torrent_hash']))
debug_msg('torrent_id: {foo}'.format(foo = TR['torrent_id']))
debug_msg('torrent_name: {foo}'.format(foo = TR['torrent_name']))
debug_msg('torrent_path: {path}'.format(path=torrent_path))

if re.search('[Ss]\d{1,2}[Ee]\d{1,2}', TR['torrent_name']):
    target_path = '/data/media/TV/1New'
else:
    target_path = '/data/media/Movies'

debug_msg('target_path: {tp}'.format(tp=target_path))

file_spec='{tpath}/*.part01.rar'.format(tpath=torrent_path)
debug_msg("Checking: {file_spec}".format(file_spec=file_spec))
files = glob.glob(file_spec)
unrar = True
if files == []:
    file_spec='{tpath}/*.rar'.format(tpath=torrent_path)
    debug_msg("Checking: {file_spec}".format(file_spec=file_spec))
    files = glob.glob(file_spec)

if files == []:
    unrar = False
    for ext in ext_list:
        file_spec='{tpath}/*.{ext}'.format(tpath=torrent_path, ext=ext)
        debug_msg("Checking: {file_spec}".format(file_spec=file_spec))
        files = glob.glob(file_spec)
        if files != []:
            break

debug_msg('files: {f}'.format(f=files))
debug_msg('unrar: {unrar}'.format(unrar=unrar))

message = 'Something weird happened: unrar'

if unrar:
    try:
        with rarfile.RarFile(files[0]) as rf:
            namelist = rf.namelist()
            debug_msg('namelist: {f}'.format(f=namelist))
            for tfile in namelist:
                debug_msg('tfile: {f}'.format(f=tfile))
                if any(word in tfile for word in ext_list):
                    debug_msg("Extracting '{tfile}' to '{dest}'".format(tfile=tfile, dest=target_path))
                    rf.extract(tfile, path=target_path)
                    message = "Extracted '{tfile}' to '{dest}'".format(tfile=tfile, dest=target_path)
    except FileNotFoundError as e:
       message = 'Exception: {e}'.format(e=e)
    except Exception as e:
       message = 'Exception: {e}'.format(e=e)
else:
    try:
        shutil.copy(files[0], target_path)
        message = "Copied {file} to {path}".format(file=files[0], path=target_path)
    except Exception as e:
        message = 'Exception: {e}'.format(e=e)

debug_msg('message: {msg}\n'.format(msg=message))

try:
    subs = glob.glob('{tpath}/*.srt'.format(tpath=torrent_path))
    if subs != []:
        shutil.copy(subs[0], target_path)
        debug_msg("Copied subtitle file: {f}".format(subs[0]))
        message += "\nCopied subtitle file: {f}".format(f=subs[0])

except Exception as e:
    message = 'Exception: {e}'.format(e=e)

debug_msg("Sending Email")

with open('/etc/transmission-daemon/sp_key') as f:
    api_key = f.read().rstrip()

sp = SparkPost(api_key)

response = sp.transmissions.send(
    recipients=['wonko@4amlunch.net', 'paula@4amlunch.net'],
    text='Torrent {torrent_name} done downloading at {time}\n\n{msg}'.format(torrent_name=TR['torrent_name'], time=TR['time'], msg=message),
    from_email='Transmission <transmission@4amlunch.net>',
    subject='Torrent {torrent_name} done'.format(torrent_name=TR['torrent_name'])
)

# client = boto3.client('ses')
# response = client.send_email(
#     Source='Transmission <transmission@4amlunch.net>',
#     Destination={
#         'ToAddresses': [
#             'wonko@4amlunch.net',
#             'paula@4amlunch.net'
#         ]
#     },
#     Message={
#         'Subject': {
#             'Data': 'Torrent {torrent_name} done'.format(torrent_name=TR['torrent_name']),
#             'Charset': 'UTF-8'
#         },
#         'Body': {
#             'Text': {
#                 'Data': 'Torrent {torrent_name} done downloading at {time}\n\n{msg}'.format(torrent_name=TR['torrent_name'], time=TR['time'], msg=message),
#                 'Charset': 'UTF-8'
#             }
#         }
#     }
# )

debug_msg(response)
debug_msg("***** Done! *****")
