Watch the TorrentLeech RSS feed and pick up shows you are interested in automatically.

Requires the module: transmissionrpc [http://pythonhosted.org/transmissionrpc/]

Quick Usage:

1) Copy tldb.conf.template to your config file name/location
2) Edit that file filling in the obvious places
3) Run: ./add_show.py -n <show name> -s <season> -e <episode>
4) Run: ./tl_rss.py

Notes:

For add_show.py the <season> and <episode> arguments are the latest that you are no longer interested in.
tl_rss.py won't fetch shows that are that season/episode or earlier.
