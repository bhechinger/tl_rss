#!/usr/bin/env python3
# import boto3
#
# client = boto3.client('ses')
#
# response = client.send_email(
#     Source='Transmission <transmission@4amlunch.net>',
#     Destination={
#         'ToAddresses': [
#             'wonko@4amlunch.net',
#         ]
#     },
#     Message={
#         'Subject': {
#             'Data': 'This is a test',
#             'Charset': 'UTF-8'
#         },
#         'Body': {
#             'Text': {
#                 'Data': 'Testing 1 2 3',
#                 'Charset': 'UTF-8'
#             }
#         }
#     }
# )
#
# print(response)

from sparkpost import SparkPost

with open('/tmp/sp_key') as f:
    api_key = f.read().rstrip()

sp = SparkPost(api_key)

response = sp.transmissions.send(
    recipients=['wonko@4amlunch.net'],
    text='Hello world>',
    from_email='test@4amlunch.net',
    subject='Hello from python-sparkpost'
)

print(response)
